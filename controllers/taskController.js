const Task = require("../models/taskModel");
const getTask = async (req, res) => {
	const { id } = req.params;

	if(!mongoose.Types.ObjectId.isValid(id)){
		return res.status(404).send({error: "No such task"});
	}

	const task = await Task.findById(id);

	if(!task){
		return res.status(404).send({error: "No such task"});
	}

	res.status(200).send(task);
};



//Controller function for updating a task
			module.exports.updateTask =(taskId, newContent) =>{

				return Task.findById(taskId).then((result, error) =>{
					if(error){
						console.log(error)
						return false
					}
						result.name = newContent.name;

						return result.save().then((updatedTask, saveErr) =>{

							if(saveErr){
								console.log(saveErr)
								return false
							}else{

								return updatedTask
							}
						})		
			})
		}
