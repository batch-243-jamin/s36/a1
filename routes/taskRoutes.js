const express = require('express')
const router = express.Router();
const taskController = require("../controllers/taskController");

//Route to get a tasks 
const {
	getTask,
	updateTask
} = require("../controllers/taskController");

//Route to Update Task
router.put("/:id",(req, res)=>{
			taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
		});

module.exports = router;
