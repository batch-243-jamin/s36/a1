const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3000;
const taskRoutes = require("./routes/taskRoutes");

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/tasks", taskRoutes);

mongoose.connect("mongodb+srv://admin:admin@batch243jamin.kuyagcd.mongodb.net/B243-to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);


app.listen(port, () => console.log(`Server Running at port ${port}`));