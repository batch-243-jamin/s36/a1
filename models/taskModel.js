//Create the Schema, model and export the file
//Import the mongoose module 
	const mongoose = require("mongoose");

//Create the Schema using the mongoose.Schema() function
	const taskSchema = new mongoose.Schema({
		name: String,
		status:{
			type:String,
			default:"pending"
		}
	});

	module.exports = mongoose.model("Task", taskSchema);
